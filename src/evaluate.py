from joblib import load
from datetime import datetime
import yaml
import pandas as pd
from sklearn.metrics import mean_squared_error

config_path = 'params/evaluate.yml'


def make_test_report(X, y_true, config, model):
    score = model.score(X, y_true)
    y_pred = model.predict(X)
    mse = mean_squared_error(y_pred, y_true, squared=False)
    report = [
        f'Evaluate date time: {datetime.now()}\n',
        f'Test data len: {len(X)}\n',
        f'R^2 is {score:.4f}\n',
        f'MSE is {mse:.2f} * 1000 RUB\n'
    ]
    with open(config['report_path'], 'w', encoding='utf-8') as file:
        file.writelines(report)


if __name__ == '__main__':
    with open(config_path) as f:
        config = yaml.safe_load(f)

    model = load(config['model_path'])
    test_df = pd.read_csv(config['test_df'])

    X = test_df['total_meters'].to_numpy().reshape(-1, 1)
    y_true = test_df['price']

    make_test_report(X, y_true, config, model)
