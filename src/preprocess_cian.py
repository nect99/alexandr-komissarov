import pandas as pd
import yaml
from sklearn.model_selection import train_test_split

config_path = 'params/preprocess_cian.yml'


def read_cian_data(config):
    in_data = config['in_data']
    df = pd.read_csv(in_data, sep=';')
    return df[['total_meters', 'price']]


if __name__ == '__main__':
    with open(config_path) as file:
        config = yaml.safe_load(file)
    df = read_cian_data(config)
    df['price'] = df['price'] / 1000
    df = df[df['total_meters'] > 0]
    train_df, test_df = train_test_split(df, test_size=0.2)
    train_df.to_csv(config['train_out'])
    test_df.to_csv(config['test_out'])
