import yaml
from joblib import load
from flask import Flask, request

app = Flask(__name__)

config_path = 'params/predict_app.yml'
with open(config_path) as f:
    config = yaml.safe_load(f)


def get_model():
    return load(config['model_path'])


@app.route("/")
def home():
    return 'Go to /predict page!'


@app.route("/predict")
def predict():
    f_1 = request.args.get("GrLivArea")
    if f_1:
        f_1 = int(f_1)
        model = get_model()
        result = model.predict([[f_1]])
        return str(f'{result[0]:.2f} RUB')
    return 'No get param provided'


if __name__ == "__main__":
    app.run()
