import unittest

from requests import request

URL = 'http://localhost:5000/predict'
PARAMS = '?GrLivArea=40'

response = request('GET', URL + PARAMS)
print(response.text)


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.url = 'http://localhost:5000'
        self.endpoint = '/predict'

    def test_home_page(self):
        response = request('GET', self.url)
        self.assertIn('Go to /predict page!', response.text)
